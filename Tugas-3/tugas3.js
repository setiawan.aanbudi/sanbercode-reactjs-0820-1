// Tugas No. 1 - Tugas - 3
console.log("------------Tugas No 1--------------");
var kataPertama = "Saya";
var kataKedua = "Senang";
var kataKetiga = "Belajar";
var kataKeempat = "Javascript";

console.log(kataPertama,kataKedua,kataKetiga,kataKeempat);

// Tugas No. 2 - Tugas - 3
console.log("------------Tugas No 2--------------");
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var angkaPertama = parseInt(kataPertama);
var angkaKedua = parseInt(kataKedua);
var angkaKetiga = parseInt(kataKetiga);
var angkaKeempat = parseInt(kataKeempat);

var jumlah = angkaPertama + angkaKedua + angkaKetiga + angkaKeempat;

console.log(jumlah);

// Tugas No.3 - Tugas - 3
console.log("------------Tugas No 3--------------");
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14)
var kataKetiga = kalimat.substring(15,18)
var kataKeempat = kalimat.substring(19,24)
var kataKelima = kalimat.substring(25,31)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima); 

// Tugas No.3 - Tugas - 4
console.log("------------Tugas No 4--------------");

var nilai = 46;

if (nilai >= 80){
    console.log("Indeks A");
} else if (nilai >= 70) {
    console.log("Indeks B");
} else if (nilai >= 60) {
    console.log("Indeks C");
} else if (nilai >= 50) {
    console.log("Indeks D");
} else if (nilai < 50 ) {
    console.log("Indeks E");
}

// Tugas No.3 - Tugas - 5
console.log("------------Tugas No 5--------------");

var tanggal = 11;
var bulan = 6;
var tahun = 1995;

var bulanString ;

switch (bulan){
    case 1 : 
        bulanString = "Januari";
    break;
    case 2 : 
        bulanString = "Februari";
    break;
    case 3 : 
        bulanString = "Maret";
    break;
    case 4 : 
        bulanString = "April";
    break;
    case 5 : 
        bulanString = "Mei";
    break;
    case 6 : 
        bulanString = "Juni";
    break;
    case 7 : 
        bulanString = "Juli";
    break;
    case 8 : 
        bulanString = "Agustus";
    break;
    case 9 : 
        bulanString = "September";
    break;
    case 10 : 
        bulanString = "Oktober";
    break;
    case 11 : 
        bulanString = "November";
    break;
    case 12 : 
        bulanString = "Desember";
    break;
}

console.log(tanggal+" "+bulanString+ " "+tahun);